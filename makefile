# Makefile for project gravtools

# Test run
test:
	$(info Test-run for this makefile!)
	$(info Yeah!!)

# Project initialization
init:
	pip install -r requirements.txt

# Convert *.ui files from Qt Designer to Python files:
py_gui:
	pyuic6 -o lifescale/gui/MainWindow.py lifescale/gui/MainWindow.ui
	pyuic6 -o lifescale/gui/dialog_about.py lifescale/gui/dialog_about.ui

# Convert QT resources to a python file using rcc of PySide2:
#  - PyQt >= 6.3.1 is required!
py_gui_resources:
	rcc -g python lifescale/gui/resources.qrc | sed '0,/PySide2/s//PyQt6/' > lifescale/gui/resources.py

# Package test (install in current virtual environment, editable install with pip)
test_pack:
	pip install -e .

# Uninstall test package
test_pack_uninstall:
	pip uninstall gravtools

# Build package with setuptools (new version):
build:
	rm -rf lifescale_utils.egg-info/
	python -m build

# Upload package to pypi.org
pypi_push:
	twine upload --verbose dist/*