# lifescale_utils
Data analysis utility tools for lifescale.

# Command line programs:

## ls2csv
*ls2csv* is a command line program that reads the content of the xlsm files written by lifescale units, parses the data and writes them to three csv 
files (where `[run-name]` is the name from the settings sheet):
  * `Masses_Vibrio_[run-name].csv`: Data series from the sheet AcquisitionIntervals.
  * `Metadata_[run-name].csv`: Data from the sheet PanelData.
  * `SampleSummary_[run-name].csv`: Data from the sheet IntervalAnalysis plus sample related data from AcquisitionIntervals.

### Usage:
```
usage: ls2csv [-h] -i INPUT_XLSM -o OUT_DIR [-nv] [-s] [-t]

Conversion from lifescale xlsm output to csv files

options:
  -h, --help            show this help message and exit
  -i INPUT_XLSM, --input-xlsm INPUT_XLSM
                        Path and name of the input xlsm file created by
                        lifescale. (default: None)
  -o OUT_DIR, --out-dir OUT_DIR
                        Output directory for the CSV files. (default: None)
  -nv, --not-verbose    Disable command line status messages. (default: False)
  -s, --sample-stats    Calculate sample statistics of masses (median, std.
                        deviation, quartiles, interquartile range) and add
                        them to the SampleSummary output CSV file (columns:
                        Mass_median, Mass_std, Mass_q25, Mass_q75,Mass_iqr).
                        (default: False)
  -t, --sort-masses-by-time
                        Sort data in the Masses CSV file by acquisition time.
                        (default: False)

```


# License and copyright

Copyright (C) 2022  Andreas Hellerschmied (<heller182@gmx.at>)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

