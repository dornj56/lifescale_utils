"""Start the lifescale GUI from here!

Copyright (C) 2022  Andreas Hellerschmied <heller182@gmx.at>
"""
from lifescale.gui.gui_main import main


def run_gui():
    """Start the GUI."""
    main()


if __name__ == "__main__":
    """Main Program."""
    run_gui()
