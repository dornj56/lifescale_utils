"""LifeScale utils is a utility program for handling data output.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

:Authors:
    Andreas Hellerschmied (heller182@gmx.at)
"""

__version__ = '0.0.6'
__author__ = 'Andreas Hellerschmied'
__git_repo__ = 'https://gitlab.com/hellerdev/lifescale_utils'
__email__ = 'heller182@gmx.at'
__copyright__ = '(c) 2023 Andreas Hellerschmied'
__pypi_repo__ = 'https://pypi.org/project/lifescale-utils/'
